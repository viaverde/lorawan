# lorawan



## Getting started

3.4.2.14 Relay -- Control Relay Output RO1/RO2

    Downlink Payload (prefix 0x03):

0x03 aa bb      // Set RO1/RO2 output

If payload = 0x030100, it means set RO1 to close and RO2 to open.

00: Close ,  01: Open , 11: No action

<pre>
Code	RO1	        RO2
030011	Open	    No Action
030111	Close	    No Action
031100	No Action   Open
031101	No Action   Close
030000	Open	    Open
030101	Close	    Close
030100	Close	    Open
030001	Open	    Close
</pre>

Device will upload a packet if downlink code executes successfully.

3.4.2.15 Relay -- Control Relay Output RO1/RO2 with time control

    AT Command:

There is no AT Command to control Relay Output

    Downlink Payload (prefix 0x05):

0x05 aa bb cc dd      // Set RO1/RO2 relay with time control

This is to control the relay output time of relay. Include four bytes:

First Byte : Type code (0x05)

Second Byte(aa): Inverter Mode

01: Relays will change back to original state after timeout.

00: Relays will change to an inverter state after timeout

Third Byte(bb): Control Method and Ports status:

image-20221008095908-1.png

Fourth/Fifth/Sixth/Seventh Bytes(cc): Latching time. Unit: ms

Note:

   Since Firmware v1.6.0, the latch time support 4 bytes and 2 bytes

   Before Firmwre v1.6.0 the latch time only suport 2 bytes.

Device will upload a packet if downlink code executes successfully.

Example payload:

1. 050111927C0

Relay1 and Relay 2 will be set to NC , last 2 seconds, then change back to original state.

2. 05011007D00

Relay1 will change to NC, Relay2 will change to NO, last 2 seconds, then both change back to original state.

3. 05000107D00

Relay1 will change to NO, Relay2 will change to NC, last 2 seconds, then relay change to NC,Relay2 change to NO.

4. 050000927C0

<pre>
050111000927C0 on 10 min
050111000493E0 on 5 min
0501110000EA60 on 1 min
0501112710 on 10 sec
05011107D0 on 2 sec

05010007D00 off 2 sec
</pre>


Relay 1 & relay2 will change to NO, last 2 seconds, then both change to NC.